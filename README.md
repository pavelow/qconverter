# Q Convertor
Uses Q notation to calculate things for fixed point, fixed precision binary numbers.

Can convert a decimal number to fixed precision, fixed point binary number with provided format in Q notation.
Decimal representation will not be accurate for a high number of fractional bits because of the underlying use of single precision floats.
