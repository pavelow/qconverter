//This function gets the form data and creates a value string
function getForm() {
	qfmtValid = validateQfmt(document.getElementById("Qfmt").value);
	nValid = validateNDec(document.getElementById("Nvalue").value) || validateNHex(document.getElementById("Nvalue").value)
	document.getElementById("Nvalue").style = "";
	document.getElementById("Qfmt").style = "";

	if(qfmtValid && nValid) {
			calculate();
	} else {
		if(!qfmtValid) {
			document.getElementById("Qfmt").style = "background-color: #ff8c8c;";
		}
		if(!nValid) {
			document.getElementById("Nvalue").style = "background-color: #ff8c8c;";
		}
	}
}

function copyValue(id) {
  var copyText = document.getElementById(id);
  copyText.select();
  copyText.setSelectionRange(0, 99999); /*For mobile devices*/
  document.execCommand("copy");
}

function validateQfmt(qfmt) {
  return qfmt.match(/^U?Q[0-9]{1,4}(\.[0-9]{1,4})?$/); //Correct form, test range
}

function validateNDec(n) {
  return (n.match(/^-?[0-9]+(\.[0-9]+)?$/)); //Correct form, test range
}

function validateNHex(n) {
  return (n.match(/^0x[0-9,a-f]*$/)); //Correct form, test range
}

function calculate() {

	//parsing
	n = document.getElementById("Nvalue").value;
	frac = parseInt(document.getElementById("Qfmt").value.match(/([0-9]+)$/)[0]);
	i_part = document.getElementById("Qfmt").value.match(/([0-9]+)(?=\.)/);
	integer = i_part ? parseInt(i_part[0]) : 0;
	signed = document.getElementById("Qfmt").value.match(/U(?=Q)/) ? false : true;
	negative = n.includes('-') ? true : false;
	resolution = 1.0/(Math.pow(2,frac));

	if (signed && document.getElementById("signBit").checked) { //Add implicit sign bit
		integer++;
	}

	if (signed && integer == 0) { //Assume not signed if there is no sign bit
		signed = false;
	}

	document.getElementById("bits").innerText = integer + frac;


	//stats
	document.getElementById("range").innerText = " " + findRange(integer, frac, signed);
	document.getElementById("res").innerText = " " + resolution;


	if(validateNHex(n)) {
		//conversion for Hex
		b_string = eval(n).toString(2);
		f_string = b_string.substring(b_string.length-frac, b_string.length)
		i_string = b_string.substring(0,b_string.length-frac)

		f_int = eval("0b"+f_string) * resolution;
		i_int = bin2dec(i_string, integer);

		if((i_int < 0 || Object.is(i_int,-0)) && f_int != 0) { // (╯°□°)╯︵ ┻━┻ because -0 == 0
			negative = true;
			f_int = 1-f_int;
		}

		n = i_int.toString() + "." + (f_int != 0 ? f_int.toString().split('.')[1] : "0");
	}

	n_fl = parseFloat(n);
	n_fc = clamp(n_fl, findMin(integer, frac, signed), findMax(integer, frac, signed));
	if(n_fc != n_fl) { //We got clamped
		n = n_fc;
	}

	if(Number.isInteger(n_fc)) {
		n = n.toString() + ".0";
	} else {
		n = n.toString();
	}

	n_i = parseInt(n.split('.')[0]);
	n_f = parseInt(n.split('.')[1]);
	l_f = n.split('.')[1].length;


	if(signed && integer > 1) {
		q_i = clamp(n_i, (-Math.pow(2,integer)/2) ,(Math.pow(2,integer)/2)-1);
	} else {
		q_i = clamp(n_i, -1, Math.pow(2,integer)-1);
		console.log(q_i);
	}

	if(negative) {
		q_m = Math.pow(2, frac) - (Math.round(n_f/(Math.pow(10, l_f)/Math.pow(2, frac))))
		q_f = 1-(q_m * resolution);
	} else {
		q_m = Math.round(n_f/(Math.pow(10, l_f)/Math.pow(2, frac)))
		q_f = q_m * resolution;
	}

	document.getElementById("value").value = (negative && q_i == 0 ? "-" : "") + (q_i + q_f);

	if(signed && negative && integer == 1) { //Special case for signed and 1 integer bit
		q_i_str = '1';
	} else {
		q_i_str = dec2bin(q_i);
	}

	q_i_str = bufferBinStr(q_i_str.substring(q_i_str.length-integer, q_i_str.length), integer);

	q_f_str = q_m.toString(2);

	q_f_str = bufferBinStr(q_f_str.substring(q_f_str.length-frac, q_f_str.length),frac);
	document.getElementById("binvalue").value = "0b" + q_i_str + '.' + q_f_str;
	document.getElementById("hexvalue").value = "0x" + eval("0b" + q_i_str + q_f_str).toString(16);
}

function findRange(integer, frac, signed) {
	ret = "";
	frac_max = ((Math.pow(2,frac)-1)/Math.pow(2,frac));
	if(!signed) {
		ret = "0.0 to +" + findMax(integer, frac, signed);
	} else {
		max = findMax(integer, frac, signed);
		min = findMin(integer, frac, signed);
		ret = min + ".0 to +" + max;
	}
	return ret;
}

function findMax(integer, frac, signed) {
	frac_max = ((Math.pow(2,frac)-1)/Math.pow(2,frac));
	if(!signed) {
		max = Math.pow(2, integer)-1 + frac_max;
	} else {
		max = (Math.pow(2, integer)/2)-1 + frac_max;
	}
	return max;
}

function findMin(integer, frac, signed) {
	if(!signed) {
		min = 0.0;
	} else {
		min = Math.pow(2, integer)/2;
	}
	return min*-1;
}


function dec2bin(dec){
    return (dec >>> 0).toString(2);
}

// two's complement conversion to signed int
function bin2dec(bin, len) {
	if(bin.length == 0) {
		bin = "0";
	}
	if(bin[0] == "1" && bin.length == len && len > 1) { //negative
		bin = bin.split('').map(b => (1 - b).toString()).join('');
		return (eval("0b"+bin)+1) * -1;
	} else if(bin[0] == "1" && len == 1) {
		return -0.0;
	}

	return eval("0b"+bin);
}

function clamp(num, min, max) {
  return num <= min ? min : num >= max ? max : num;
}

function bufferBinStr(str, len) {
	z = '0';
	if(str.length < len) {
		str = z.repeat(len-str.length) + str;
	}
	return str;
}
